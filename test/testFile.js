const add = require("../index");
const assert = require("assert");

describe("Assert the addition of two numbers", () => {
    // asserting that 5 + 7 = 12 
    it("5 + 7 = 12", () => {
        assert.equal(12, add(5, 7));
    });

    // asserting that 5 + 7 != 57
    it("5 + 7 != 57", () => {
        assert.notEqual("57", add(5, 7));
    });
});