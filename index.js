// we import log4js
const log4js = require('log4js')

// logger allows us to register what is going on
const logger = log4js.getLogger()

// level is the kind of execution that is going on
logger.level = 'debug' // we can change this to production

// we can personalize the messages
logger.info('App launched successfully, congrats!')
logger.warn('Warning! Missing a library!')
logger.error("Couldn't find function!")
logger.fatal("We couldn't start the application")

function add (x, y) {
  return x + y
}

module.exports = add

console.log('Hello from index.js!')
